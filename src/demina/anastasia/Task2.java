package demina.anastasia;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class Task2 {
    public static void main(String[] args) throws IOException {
        createFile();

        // часть с возрастанием
        doTask(true);
        // часть с убыванием
        doTask(false);
    }

    private static void doTask(boolean growingOrder) throws IOException {
        ArrayList numbers = readFile();
        if (growingOrder){
            Collections.sort(numbers);
            System.out.println(numbers);
        } else {
            Collections.sort(numbers, Collections.reverseOrder());
            System.out.println(numbers);
        }

    }

    //создаем файл
    private static void createFile() throws IOException {
        ArrayList userArray = createArray();
        FileWriter userFile = new FileWriter("file.txt");
        for (int i = 0; i < userArray.size(); i++) {
            userFile.write(userArray.get(i).toString());
            userFile.write(',');
        }
        userFile.flush();
        userFile.close();
    }

    //читаем
    private static ArrayList readFile() throws IOException {
        FileReader userFile = new FileReader("file.txt");
        ArrayList numbersSort = new ArrayList();
        Scanner scan = new Scanner(userFile);
        scan.useDelimiter(",");
        while (scan.hasNextInt()) {
            numbersSort.add(scan.nextInt());
        }
        userFile.close();
        return numbersSort;
    }

    //создаем массив со значениями от 1 до 20 и перемешиваем
    private static ArrayList createArray() {
        ArrayList numbers = new ArrayList();
        for (int i = 0; i <= 20; i++) {
            numbers.add(i);
        }
        Collections.shuffle(numbers);
        return numbers;
    }
}