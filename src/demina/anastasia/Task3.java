package demina.anastasia;

public class Task3 {
    public static void main(String[] args) {
        long resultNumber=factorial20();
        System.out.println("20! = " + resultNumber);
    }

    private static long factorial20(){
        long resultNumber = 1;
        for (int i=1; i<=20; i++){
            resultNumber=resultNumber*i;
        }
        return resultNumber;
    }
}
